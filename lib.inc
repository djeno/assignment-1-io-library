%define EXIT_SYSCALL 60

%define PRINT_SYSCALL 1
%define STDOUT 1

%define READ_SYSCALL 0
%define STDIN 0

%define TAB_CHAR 0x9
%define NEWLINE_CHAR 0xA
%define SPACE_CHAR 0x20


section .text 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_SYSCALL
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax  
	.loop:
		cmp  byte[rdi + rax], 0
        je .end
        inc rax			; длина хранится в rax 
        jmp .loop
	.end:
		 ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi 			
    call string_length
    mov rdx, rax 		
    pop rsi 			
    mov rax, PRINT_SYSCALL
    mov rdi, STDOUT
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE_CHAR

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi      ; сохраняем код символа  
    mov rsi, rsp
    mov rax, PRINT_SYSCALL    ; кладем номер системного вызова для write
    mov rdi, STDOUT    ; кладем номер файлового дескриптора
    mov rdx, 1    ; кладем количество байт для записи
    syscall
    pop rdi
	ret


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r10, 10 ; делитель
    mov rcx, rsp  
    sub rsp, 32 ; 
    dec rcx
    mov byte [rcx], 0
    .loop:
        xor rdx, rdx
        div r10 ; остаток от деления хранится в rdx
        add rdx, '0' ; получаем код символа в ASCII, путем добавления остатка от деления к коду символа нуля
        dec rcx
        mov byte[rcx], dl
        cmp rax, 0
        je .print
        jmp .loop
    .print:
        mov rdi, rcx
        call print_string
        add rsp, 32	
        ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi   ; Проверка на знак числа, если положительное, то сразу печатаем
    jns .positive
	push rdi
	mov rdi, '-' 
    call print_char  
	pop rdi
	neg rdi
    .positive:
    	call print_uint ; печатаем число
    	ret 

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor ecx, ecx  ; счетчик символов
.compare_loop:
    lodsb         ; загружаем следующий байт из первой строки  в al 
    cmp al, [edi] ; сравниваем байт из 1 строки с текущим байтом из 2
    jne .unequal   ; если байты не равны
    cmp al, 0     
    je .equal      ; перейти к метке equal, если обе строки равны
    inc edi       ; увеличиваем указатель на 2 строку
    loop .compare_loop
    jmp .unequal   ; если разницу не нашли, строки имеют разную длину и не равны
.equal:
    mov eax, 1    ; строки равны, возвращаем 1
    jmp .end
.unequal:
    xor eax, eax  ; строки не равны, возвращаем 0
.end:
    ret 

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char: 
    push 0 
    mov rdi, STDIN              
    mov rax, READ_SYSCALL
    mov rsi, rsp                   
    mov rdx, 1            
    syscall
    pop rax                 
    ret
    

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	push rdi        ; сохраняем адрес буфера
	push r12
	push r13
	push r14
	mov r12, rdi
	mov r13, rsi
	mov r14, rdx
	.loop:          ; читаем символы
        call read_char
        cmp al, NEWLINE_CHAR
        jz .space
        cmp al, TAB_CHAR
        jz .space
        cmp al, SPACE_CHAR
        jz .space
        cmp al, 0
        jz .end
        jmp .append
    .space:         
        cmp r14, 0
        jz .loop
        jmp .end
    .append:          ; сохраняем полученный символ в буфер
        mov byte[r12+r14], al
        inc r14
        cmp r13, r14
        jz .failure 
        jmp .loop
        jmp .end
    .failure:        ; буфер переполнен
        pop r14
        pop r13
        pop r12
        pop rdi
        xor rax, rax ; возвращаем 0
        ret
    .end:
		mov rdx, r14
		pop r14
        pop r13
        pop r12
        pop rax              ; восстанавливаем адрес буфера 
        mov byte[rax+rdx], 0 ; добавляем нуль-терминатор к слову
        ret
    

	
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rcx, rcx
    xor rax, rax
    mov r8, 10      ; основание сс
    xor r9, r9      ; индекс в строке 
    .main_loop:
        mov cl, byte[rdi+r9]
        sub cl, '0'
        cmp cl, 0
        jl .end
        cmp cl, 9     ; проверка, что символ находится в диапазоне от 0 до 9
        jg .end
        inc r9
        mul r8
        add rax, rcx
        jmp .main_loop
    .end:
        mov rdx, r9
        ret
	   
        
        
        
        
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push rdi 
    mov al, byte[rdi] 
    cmp al, '+'  ; Если число c плюсом, то пропускаем + и переходим к обработке положительных чисел
    je .plus
    cmp al, '-'  ; если число не отрицательное, то сразу парсим вызовом функции
    jne .positive  
    inc rdi  
    call parse_uint  
    cmp rdx, 0
    jz .fault
    neg rax  
    inc rdx  
    jmp .end
.plus:
	inc rdi     
.positive:
    call parse_uint  
    test rdx, rdx  
    jnz .end  
.fault:
    xor rdx, rdx
.end:
    pop rdi
    ret
        
       
     

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx
    .loop:
        mov al, byte[rdi]
        inc rdi                         
        inc rcx                         
        cmp rcx, rdx                    
        jg .failure    ; если счетчик больше макс размера, неудача                  
        mov byte[rsi], al               
        inc rsi
        cmp al, 0      ; проверяем, достигли ли нуль-терминатора 
        je .success
        jmp .loop
    .failure:
        mov rax, 0                     
        jmp .end
    .success:
        mov rax, rcx                    
    .end:
        ret
